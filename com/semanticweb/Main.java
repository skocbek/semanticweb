package com.semanticweb;


/**
 * Main class for testing.
 */
public class Main {

    public static void main(String[] args) {

        // First problem testing.
        Problem1 p1 = new Problem1();

        if (p1.writeFrequenciesToJson("test.json"))
            System.out.println("Digits calculated and written in json successfully.");
        else
            System.out.println("There was an error. See previous messages.");

        // Second problem testing.
        Problem2 p2 = new Problem2();
        System.out.println(p2.getIps("25525511135"));
        System.out.println(p2.getIps("0000"));
        System.out.println(p2.getIps("1001"));
        System.out.println(p2.getIps("111111"));
        System.out.println(p2.getIps("25525500"));
        System.out.println(p2.getIps("25525600"));
        System.out.println(p2.getIps("255255255255"));
        System.out.println(p2.getIps("256255255255"));
        System.out.println(p2.getIps("355255255255"));
        System.out.println(p2.getIps("35255255255"));
    }
}
