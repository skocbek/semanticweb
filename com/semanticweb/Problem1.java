package com.semanticweb;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;

/**
 * Created by simon on 31/03/2018.
 */

// Count frequency of digits in first 1000 digits of pi and write into json.
public class Problem1 {

    /**
     * Calculates pi. We just simply read pi from a file, which contains pi value with 1000 digits, that were
     * calculated with perl script externally:
     *
     * perl -Mbignum=bpi -wle "print bpi(1000)"
     *
     * We save output to a file, which is then used here.
     *
     * This is not the most elegant solution, and could also be done with ProcessBuilder or Java code with, e.g.,
     * implementing BBP formula to get n-th digit of pi (https://www.math.hmc.edu/funfacts/ffiles/20010.5.shtml)
     *
     * @return pi value as String, null on error
     */
    private String calculatePi() {

        String pi = null;
        try {

            String piFileName = "src/com/semanticweb/pi1000.txt";
            BufferedReader reader = new BufferedReader(new FileReader(piFileName));
            pi = reader.readLine();
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return pi;
    }

    /**
     * Counts frequencies for first 1000 digits of pi.
     * @return array where index is a digit, and value is the digit's frequency, null on error.
     */
    private int [] countFrequencies() {

        // Maximum freq for each digit can be 1000, so MAX_VALUE of int should be enough.
        int[] frequencies = new int[10];

        String pi = calculatePi();

        if (pi == null)
            return null;

        for (int i = 0; i < pi.length(); i++){
            char digit = pi.charAt(i);

            // Dot should be ignored.
            if (Character.isDigit(digit)) {
                frequencies[Character.getNumericValue(digit)] += 1;
            }
        }

        return frequencies;
    }

    /**
     *
     * @param fileName file name to write too.
     * @return true on success, false on error.
     */
    public boolean writeFrequenciesToJson(String fileName) {

        try {
            int [] frequencies = countFrequencies();

            if (frequencies == null)
                return false;

            // Creating json string. This could also be done with external library like Google's gson, however, since
            // the output is pretty simple, and we do not want to rely on external libraries, we use this approach.

            String json = "{";
            for (int i = 0; i < frequencies.length; ++i)
                if (frequencies[i] > 0)
                    json += "\"" + i + "\":" + frequencies[i] + ",";

            json = json.substring(0, json.length() - 1) + "}";
            PrintWriter out = new PrintWriter(fileName);
            out.println(json);
            out.close();

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
