package com.semanticweb;

import java.util.ArrayList;

/**
 * Created by simon on 31/03/2018.
 */

// Given a string containing only digits, restore it by returning all possible valid IP address combinations.
// Given "25525511135", return ["255.255.11.135", "255.255.111.35"]. We consider only IPv4 addresses.
public class Problem2 {

    private boolean isValid(String ip) {
        String [] ipParts = ip.split("\\.");

        // String has to start with 1, according to instructions, others can be 0, so we check the first part
        // separately.
        if (Integer.parseInt(ipParts[0]) < 1)
            return false;

        for(String ipPart : ipParts) {

            if (ipPart.startsWith("00"))
                return false;

            if (Integer.parseInt(ipPart) < 0 || Integer.parseInt(ipPart) > 255)
                return false;
        }
        return true;
    }

    /**
     *
     * @param input only digits are expected.
     * @return valid ip addresses, empty if no address is found (invalid input string).
     */
    public ArrayList<String> getIps(String input) {

        ArrayList<String> validIps = new ArrayList<String>();

        // Minimum allowed string is of length 4 (i.e., 1001).
        int minLength = 4;
        int maxLength = 12;

        // Checking for invalid strings, input string is expected to have only digits, so we do not check for that, but
        // we check for length and correct start of the string.
        if (input.length() < minLength || input.length() > maxLength || input.startsWith("0"))
            return validIps;


        // 3 loops, not very elegant, however, inputs are not long.
        String recommendedIP = "";
        for(int i = 3; i < input.length(); ++i)
            for (int j = 2; j < i; ++j)
                for (int k = 1; k < j; ++k) {
                    recommendedIP = input.substring(0, k) + "." + input.substring(k, j) + "."
                            + input.substring(j, i) + "." + input.substring(i);

                    if(isValid(recommendedIP))
                        validIps.add(recommendedIP);
                }

        return validIps;
    }
}
